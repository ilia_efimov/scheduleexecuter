package com.hometask.schedulexec;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.*;

/**
 * Created by Ilia on 26.04.2017.
 */
public class ScheduleMonitorExecuter extends Thread {

    private static final String TIME_ZONE = "UTC";
    private static final String MOSCOW_TIME_OFFSET = "+03:00:00";
    private static final int    THREAD_POOL_SIZE = 10;

    private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(THREAD_POOL_SIZE);
    private ZoneId zoneId = ZoneId.ofOffset(TIME_ZONE, ZoneOffset.of(MOSCOW_TIME_OFFSET));

    public ScheduleMonitorExecuter() {

    }


    public ScheduleMonitorExecuter(ZoneId pZoneId) {
        this.zoneId = pZoneId;
    }

    public void addTask(Entry entry) {
        if (entry == null) {
            return;
        }
        if (entry.getDateTime().compareTo(LocalDateTime.now(zoneId)) <= 0) {
            executorService.submit(entry.getCallable());
            return;
        }
        LocalDateTime currentTime = LocalDateTime.now(zoneId);
        LocalDateTime taskTime = entry.getDateTime();
        executorService.schedule(entry.getCallable(), currentTime.until(taskTime, ChronoUnit.MILLIS), TimeUnit.MILLISECONDS);
    }
}
