package com.hometask.schedulexec;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

public class App {

    private static final String TIME_ZONE = "UTC";
    private static final String SARATOV_TIME_OFFSET = "+04:00:00";

    private static List<Entry> createTasks() {
        List<Entry> tasks = new LinkedList<Entry>();

        Callable<Integer> task1 = new Callable<Integer>() {
            public Integer call() throws Exception {
                System.out.println(String.format("Task 1 executed in time %s", LocalDateTime.now()));
                return 1;
            }
        };
        LocalDateTime task1DateTime = LocalDateTime.now().plusSeconds(30);


//==================================================================================================================//

        Callable<Integer> task2 = new Callable<Integer>() {
            public Integer call() throws Exception {
                System.out.println(String.format("Task 2 executed in time %s", LocalDateTime.now()));
                return 2;
            }
        };
        LocalDateTime task2DateTime = LocalDateTime.now().plusSeconds(31);
//==================================================================================================================//
        Callable<Integer> task3 = new Callable<Integer>() {
            public Integer call() throws Exception {
                System.out.println(String.format("Task 3 executed in time %s", LocalDateTime.now()));
                return 3;
            }
        };
        LocalDateTime task3DateTime = LocalDateTime.now().plusSeconds(32);
//==================================================================================================================//
        Callable<Integer> task4 = new Callable<Integer>() {
            public Integer call() throws Exception {
                System.out.println(String.format("Task 4 executed in time %s", LocalDateTime.now()));
                return 4;
            }
        };

        LocalDateTime task4DateTime = LocalDateTime.now().plusSeconds(33);
//==================================================================================================================//
        Callable<Integer> task5 = new Callable<Integer>() {
            public Integer call() throws Exception {
                System.out.println(String.format("Task 5 executed in time %s", LocalDateTime.now()));
                return 5;
            }
        };
        LocalDateTime task5DateTime = LocalDateTime.now().plusSeconds(34);
//==================================================================================================================//

        tasks.add(new Entry(task2DateTime, task2));
        tasks.add(new Entry(task3DateTime, task3));
        tasks.add(new Entry(task5DateTime, task5));
        tasks.add(new Entry(task4DateTime, task4));
        tasks.add(new Entry(task1DateTime, task1));

        return tasks;
    }

    public static void main(String[] args) {
        System.out.println("Begin ...");
        List<Entry> tasks = createTasks();

        ScheduleMonitorExecuter executer = new ScheduleMonitorExecuter(ZoneId.ofOffset(TIME_ZONE, ZoneOffset.of(SARATOV_TIME_OFFSET)));

        for (Entry task : tasks) {
            executer.addTask(task);
        }
    }
}
