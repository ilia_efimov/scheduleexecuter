package com.hometask.schedulexec;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.concurrent.Callable;

/**
 * Created by Ilia on 26.04.2017.
 */
public class Entry  {
    private LocalDateTime dateTime;
    private Callable callable;

    public Entry(LocalDateTime pLocalDateTime, Callable pCallable) {
        this.dateTime = pLocalDateTime;
        this.callable = pCallable;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Callable getCallable() {
        return callable;
    }

    public void setCallable(Callable callable) {
        this.callable = callable;
    }
}
